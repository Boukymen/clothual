
var messages = ["Salut ! je suis ici pour t'aider à prendre en main Clothual ! Je peux aussi t'aider à choisir tes vêtements.",
				"Ici, tu auras accès à ton panier, tu pourras commander des articles ou les supprimer de ton panier",
				"Essaye le vêtement qui te plaît en le sélectionnant",
				"Avec le micro tu peux faire tes choix en parlant et je pourrai te parler en retour",
				"Prends toi en photo pour montrer ton essayage à tes amis ou tes proches !",
				"Ajoute l'article que tu portes dans tes favoris pour les retrouver plus tard",
				"Crée ton profil pour nous aider à te proposer des styles, pour nous renseigner ta taille et d'autres informations"];
var imgPath = "../img/C-max.png";
var assistantOn = 0;
$(document).ready(function(){
  $('.bubble').hide();
  $('.bubble-bottom-left').hide();
	$("#assistant2").click(function(){
	if(assistantOn==0){
		assistantOn = 1;
  		$('<img id="assistant" src="'+ imgPath +'" >').on('load',function() {
  			$(this).appendTo('#assistant_image');
  		});
      $('.bubble').show();
      $('.bubble-bottom-left').show();
  		$('<p class="message">'+messages[0]+'</p>').appendTo('#bubble');
  		setTimeout(function() {
  			$('.message').remove();
        $('.bubble').hide();
      $('.bubble-bottom-left').hide();
			}, 3000);

      
  	}
  	else{
  		assistantOn=0;
  		$("#assistant_image img:last-child").remove();
      $("#bubble").empty();
      $('.bubble').hide();
      $('.bubble-bottom-left').hide();
  	}});
  	$("#photo").hover(function(){
  		if(assistantOn==1){
        $('.bubble').show();
      $('.bubble-bottom-left').show();
  			$('#bubble').text(messages[4]);
  		}
  	});
  	$("#favoris").hover(function(){
  		if(assistantOn==1){
        $('.bubble').show();
      $('.bubble-bottom-left').show();
  			$('#bubble').text(messages[5]);
  		}
  	});
  	$("#profil").hover(function(){
  		if(assistantOn==1){
        $('.bubble').show();
      $('.bubble-bottom-left').show();
  			$('#bubble').text(messages[6]);
  		}
  	});
  	$("#panier").hover(function(){
  		if(assistantOn==1){
        $('.bubble').show();
      $('.bubble-bottom-left').show();
  			$('#bubble').text(messages[1])
  		}
  	});
	$("#micro").hover(function(){
  		if(assistantOn==1){
        $('.bubble').show();
      $('.bubble-bottom-left').show();
  			$('#bubble').text(messages[3])
  		}
  	});
  	  	

});

