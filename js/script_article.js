
// document.querySelector('.fav').addEventListener('click', function() {
//     const icon = this.querySelector('i');
//     // const text = this.querySelector('span');
  
//     if (icon.classList.contains('far')) {
//       icon.classList.remove('far');
//       icon.classList.add("fas");
//     //   text.innerHTML = 'Hide';
//     } else {
//       icon.classList.remove('fas');
//       icon.classList.add("far");
//     //   text.innerHTML = 'Show';
//     }
//   });

  $(".fav").click(function(event) {
    //   $(this).find(i).
    $(this).find('i').toggleClass('fas far');

});


$('.fav').click(function(){
  if (this.querySelector('i').classList.contains('fas')){
    $('.alert').find('.msg').text("ajout: votre article a été ajouter au favoris!");
    $('.alert').addClass("show");
    $('.alert').removeClass("hide");
    $('.alert').addClass("showAlert");
  }else {
    $('.alert').find('.msg').text("suppression: votre article a été supprimer des favoris!");
    $('.alert').addClass("show");
    $('.alert').removeClass("hide");
    $('.alert').addClass("showAlert");
  }
  setTimeout(function(){
    $('.alert').removeClass("show");
    $('.alert').addClass("hide");
  },3500);
});

$('.close-btn').click(function(){
  $('.alert').removeClass("show");
  $('.alert').addClass("hide");
});

$('.rating input').change(function () {
  var $radio = $(this);
  // $('.rating .selected').removeClass('selected');
  $radio.closest('label').addClass('selected');
  $('.alert').find('.msg').text("Merci d'avoir notée notre article !!");
    $('.alert').addClass("show");
    $('.alert').removeClass("hide");
    $('.alert').addClass("showAlert");
    setTimeout(function(){
      $('.alert').removeClass("show");
      $('.alert').addClass("hide");
    },3500);
});

function scrollToTop() {
  $(window).scrollTop(0);
}