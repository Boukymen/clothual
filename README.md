# Projet Clothual

Membres du groupe :

- BENOIT Raphaël - r7benoit@enib.fr   
- BRISSON Arnaud - a7brisso@enib.fr
- ESPARVIER Alexis - a7esparv@enib.fr
- HARKATI Amna - a6harkat@enib.fr
- KONATE Bakary - b0konate@enib.fr


Responsable du Module : Sébastien KUBICKI                              Semestre S7P 2021

Lien du drive du projet : https://drive.google.com/drive/folders/1AwdpE5ZGzjEsV9VbZvIHyOuayMl_XMoh?usp=sharing

## Présentation projet

L’objectif de ce projet serait de fournir une interface à des clients de magasin de vêtements pour qu’ils puissent essayer des vêtements sans les porter (virtuellement). À l'aide de cette interface, les utilisateurs pourraient essayer un grand nombre de produits, savoir à quoi s’attendre lorsqu’ils  les achètent. Pour le magasin, cette possibilité leur permettrait de continuer leur activité pendant une crise sanitaire. Cela éviterait aussi de créer toutes les dispositions sanitaires mises en place pour éviter de propager un virus. De plus, pour le client,  cela permettrait de passer des commandes assez facilement et d'être précis dans ses choix (faible pourcentage de retour du produit après achat).
Le but est aussi de placer le système directement dans le magasin afin d’aiguiller l’utilisateur sur ses potentiels choix à l’aide de recommandations. Il aurait alors la possibilité de choisir ce qu’il préfère et le système lui indiquerait dans quel rayon le produit se situe. Il pourrait observer quel produit lui correspond le mieux sans même devoir l’essayer. 

## Organisation du projet 

Nous avons commencé par travailler durant 4 semaines sur la partie CCU de ce projet. Nous avons créé des personnas, effectué des interviews ainsi qu'ellaboré des wireframes de notre future interface. Notre équipe a choisi de se pencher sur l'interface du miroir qui est placé dans les cabines d'essayages des magasins. Maintenant, nous allons travailler sur la partie "Front" du projet Clothual. Pour se faire, nous utiliserons les langages HTML, CSS, JS ainsi que le framework Bootstrap. Toute notre première partie de projet se trouve sur un drive et tout ce qui est code se trouve sur ce GitLab.

## Organisation du groupe

Nous avons choisi de nommer Amna chef de groupe car elle est à l'aise avec les outils GitLab ainsi que Bootstrap. De plus, elle motive le groupe à travailler en équipe.  
Nous allons répartir les différentes pages à chaque memebres du groupe afin que tout le monde puisse toucher chaque langage. 
Voici la répartition des pages : 
- Alexis : Catégories + Panier
- Amna : Accueil + Navigation
- Arnaud : Profil + A propos + Favoris
- Bakary : Articles + S'enregistrer
- Raphaël : Introduction + Assistant

## Parcours utilisateur

Nous avons décidé de commencer en travaillant sur le parcours utilisateur du projet afin de voir le projet dans sa globalité et de se répartir les tâches. Voici le lien du document : https://app.creately.com/diagram/5mnYkVa5Zho

## Structure

Comme dit précedemment, nous nous sommes répartis le travail à faire. Toutes les pages sont expliquées avec des captures d'écran et des commentaires dans le dossier ProjetClothualWEB.pdf. 

## Bilan du projet 

Ce projet a été très intéressant. En effet, nous avons eu l’opportunité de créer un projet de A à Z. Commencer par le point de vue utilisateur pour finir sur l’aspect design de l’IHM nous a beaucoup plu. Il était toutefois assez compliqué de tout mettre en commun. En effet, chacun a travaillé de son côté sur ses pages. Donc certains ont bien respecté la mise en page et le thème tandis que d’autres ont fait plus leur style à eux. Ce genre de projet doit être pensé tous ensemble afin d’arriver sur un site cohérent et non avec 5 types de pages différentes. Pour finir, nous avons apprécié travailler sur ce projet afin de développer notre créativité web tout en apprenant de nouvelles compétences.

